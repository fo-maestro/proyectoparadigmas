package com.proyectoparadigmas.graphics;

import javafx.scene.canvas.Canvas;
import javafx.scene.image.WritableImage;

/**
 * Clase Base para la Creacion de Objetos Graficos.
 */
public abstract class GraphicsObject {

    /**
     * Ancho del Objeto Grafico.
     */
    protected double width;

    /**
     * Largo del Objeto Grafico.
     */
    protected double height;

    /**
     * Crea una nueva Instancia de GraphicsObject.
     * @param width Ancho del Objeto Grafico
     * @param height Largo del Objeto Grafico
     */
    public GraphicsObject(double width, double height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Obtiene el Ancho del Objeto Grafico.
     * @return Ancho del Objeto Grafico
     */
    public double getWidth() {
        return width;
    }

    /**
     * Obtiene el Largo del Objeto Grafico.
     * @return Largo del Objeto Grafico
     */
    public double getHeight() {
        return height;
    }

    /**
     * Redimensiona el Ancho del Objeto Grfico.
     * @param width Ancho del Objeto Grafico
     */
    public void setWidth(double width) { this.width = width; }

    /**
     * Redimensiona el Largo del Objeto Grafico.
     * @param height Largo del Objeto Grafico
     */
    public void setHeight(double height) { this.height = height; }

    /**
     * Obtiene el Componente Grafico en formato de imagen del Objeto Dibujado.
     * @return Objeto Dibujado en formato Imagen
     */
    public WritableImage getGraphics() {
        return Blend.transparency(draw().snapshot(null, new WritableImage((int)width, (int)height)));
    }

    /**
     * Dibuja el Objeto Grafico.
     * @return Objeto Grafico
     */
    public abstract Canvas draw();

    //resize Method here
}
