package com.proyectoparadigmas.graphics;

import com.proyectoparadigmas.fxutils.EventDispatchListener;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * Crea un Objeto Grafico(Fosforo).
 */
public class ItemMatch extends ImageView {

    private GraphicsObject mMatch;

    /**
     * Crea una instancia de ItemMatch.
     * @param width Ancho del Objeto
     * @param height Largo del Objeto
     */
    public ItemMatch(double width, double height) {
        mMatch = new Match(width, height);
        super.setImage(mMatch.getGraphics());
    }

    public ItemMatch(GraphicsObject mMatch) {
        this.mMatch = mMatch;
        super.setImage(mMatch.getGraphics());
    }

    /**
     * Crea una instancia de ItemMatch.
     * @param width Ancho del Objeto
     * @param height Largo del Objeto
     * @param degree Angulo de rotacion del Objeto
     */
    public ItemMatch(double width, double height, double degree) {
        mMatch = new Match(width, height);
        super.setImage(mMatch.getGraphics());
        super.setRotate(degree);
    }

    public ItemMatch(GraphicsObject mMatch, double degree) {
        this.mMatch = mMatch;
        super.setImage(mMatch.getGraphics());
        super.setRotate(degree);
    }

    /**
     * Crea una instancia de ItemMatch.
     * @param x Posicion en X del Objeto
     * @param y Posicion en Y del Objeto
     * @param width Ancho del Objeto
     * @param height Largo del Objeto
     */
    public ItemMatch(double x, double y, double width, double height) {
        mMatch = new Match(width, height);
        super.setImage(mMatch.getGraphics());
        super.setX(x);
        super.setY(y);
    }

    public ItemMatch(double x, double y, GraphicsObject mMatch) {
        this.mMatch = mMatch;
        super.setImage(mMatch.getGraphics());
        super.setX(x);
        super.setY(y);
    }

    /**
     * Crea una instancia de ItemMatch.
     * @param x Posicion en X del Objeto
     * @param y Posicion en Y del Objeto
     * @param width Ancho del Objeto
     * @param height Largo del Objeto
     * @param degree Angulo de rotacion del Objeto
     */
    public ItemMatch(double x, double y, double width, double height, double degree) {
        mMatch = new Match(width, height);
        super.setImage(mMatch.getGraphics());
        super.setX(x);
        super.setY(y);
        super.setRotate(degree);
    }

    public ItemMatch(double x, double y, GraphicsObject mMatch, double degree) {
        this.mMatch = mMatch;
        super.setImage(mMatch.getGraphics());
        super.setX(x);
        super.setY(y);
        super.setRotate(degree);
    }

    /**
     * Añade un Evento Mouse Clicked.
     * @param listener Funcion CallBack
     */
    public void setOnMouseClicked(EventDispatchListener<? super MouseEvent, ? super ItemMatch> listener) {
        super.setOnMouseClicked(event -> listener.onDispatch(event, this));
    }

    public void setOnMousePressed(EventDispatchListener<? super MouseEvent, ? super ItemMatch> listener) {
        super.setOnMousePressed(event -> listener.onDispatch(event, this));
    }

    public void setOnMouseDragged(EventDispatchListener<? super MouseEvent, ? super ItemMatch> listener) {
        super.setOnMouseDragged(event -> listener.onDispatch(event, this));
    }

    public void setOnMouseReleased(EventDispatchListener<? super MouseEvent, ? super ItemMatch> listener) {
        super.setOnMouseReleased(event -> listener.onDispatch(event, this));
    }

    /**
     * Obtiene el Ancho del Objeto.
     * @return Ancho del Objeto
     */
    public double getWidth() { return mMatch.getWidth(); }

    /**
     * Obtiene el Largo del Objeto.
     * @return Largo del Objeto
     */
    public double getHeight() { return mMatch.getHeight(); }

    /**
     * Redimensiona el Ancho del Objeto.
     * @param width Ancho del Objeto
     */
    public void setWidth(double width) {
        mMatch.setWidth(width);
        super.setImage(mMatch.getGraphics());
    }

    /**
     * Redimensiona el Largo del Objeto.
     * @param height Largo del Objeto
     */
    public void setHeight(double height) {
        mMatch.setHeight(height);
        super.setImage(mMatch.getGraphics());
    }

    public Bounds getBounds() {
        return new BoundingBox(getX(), getY(), getWidth(), getHeight());
    }

    public void rotate() {

    }
}
