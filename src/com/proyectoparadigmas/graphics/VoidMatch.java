package com.proyectoparadigmas.graphics;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Created by felipe on 24-12-16.
 */
public class VoidMatch extends GraphicsObject {

    /**
     * Crea una nueva Instancia de GraphicsObject.
     *
     * @param width  Ancho del Objeto Grafico
     * @param height Largo del Objeto Grafico
     */
    public VoidMatch(double width, double height) {
        super(width, height);
    }

    @Override
    public Canvas draw() {
        Canvas canvas = new Canvas(width, height);
        GraphicsContext context = canvas.getGraphicsContext2D();
        context.setFill(Color.LIGHTGRAY);
        context.fillRect(width / 5, width + (width / 3), width - 2 * (width / 5), height - (width + (width / 3)));
        context.setFill(Color.LIGHTGRAY);
        context.fillOval(0, 0, width, width + (width / 2));

        return canvas;
    }
}
