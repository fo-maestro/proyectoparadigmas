package com.proyectoparadigmas.graphics;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Crea los Graficos de un Objeto(Fosforo).
 */
public class Match extends GraphicsObject {

    public Match(double width, double height) {
        super(width, height);
    }

    /**
     * Dibuja el Objeto Grafico.
     * @return Lienzo con el Objeto Grafico dibujado
     */
    @Override
    public Canvas draw() {
        Canvas canvas = new Canvas(width, height);
        GraphicsContext context = canvas.getGraphicsContext2D();
        context.setFill(Color.SANDYBROWN);
        context.fillRect(width / 5, width + (width / 3), width - 2 * (width / 5), height - (width + (width / 3)));
        context.strokeRect(width / 5, width + (width / 3), width - 2 * (width / 5), height - (width + (width / 3)));
        context.setFill(Color.RED);
        context.fillOval(0, 0, width, width + (width / 2));
        context.strokeOval(0, 0, width, width + (width / 2));

        return canvas;
    }
}
