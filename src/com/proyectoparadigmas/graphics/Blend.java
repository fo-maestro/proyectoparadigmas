package com.proyectoparadigmas.graphics;

import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * Created by felipe on 22-12-16.
 */
public class Blend {
    public static WritableImage transparency(WritableImage src) {
        PixelReader imageReader = src.getPixelReader();

        WritableImage dest = new WritableImage((int)src.getWidth(), (int)src.getHeight());
        PixelWriter writer = dest.getPixelWriter();

        for (int x = 0; x < src.getWidth(); x++) {
            for (int y = 0; y < src.getHeight(); y++) {
                Color imageColor = imageReader.getColor(x, y);

                if (!imageColor.equals(Color.WHITE)) {
                    writer.setColor(x, y, imageColor);
                }
            }
        }

        return dest;
    }
}
