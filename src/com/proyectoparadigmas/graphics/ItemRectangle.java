package com.proyectoparadigmas.graphics;

import com.proyectoparadigmas.fxutils.EventDispatchListener;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by felipe on 17-12-16.
 */
public class ItemRectangle extends Rectangle {

    private double degree;

    public ItemRectangle(double width, double height) {
        super(width, height);
    }

    public ItemRectangle(double x, double y, double width, double height) {
        super(x, y, width, height);
        setStroke(Color.BLACK);
    }


    public void setOnMouseDragged(EventDispatchListener<? super MouseEvent, ? super ItemRectangle> listener) {
        super.setOnMouseDragged(event -> listener.onDispatch(event, this));
    }

    public void setOnMouseReleased(EventDispatchListener<? super MouseEvent, ? super ItemRectangle> listener) {
        super.setOnMouseReleased(event -> listener.onDispatch(event, this));
    }

    public void setOnMousePressed(EventDispatchListener<? super MouseEvent, ? super  ItemRectangle> listener) {
        super.setOnMousePressed((event) -> listener.onDispatch(event, this));
    }

    public Bounds getBounds() {
        return new BoundingBox(getX(), getY(), getWidth(), getHeight());
    }

    public void rotate() {
        double width = getWidth();
        double height = getHeight();
        super.setWidth(height);
        super.setHeight(width);

        if (degree == 0) {
            degree = 90;
            super.setX(super.getX() - super.getWidth() / 2 + super.getHeight() / 2);
            super.setY(super.getY() + super.getWidth() / 2 - super.getHeight() / 2);
        } else {
            degree = 0;
            super.setX(super.getX() + super.getHeight() / 2 - super.getWidth() / 2);
            super.setY(super.getY() - super.getHeight() / 2 +  super.getWidth() / 2);
        }
    }

    public double getRotation() {
        return degree;
    }
}
