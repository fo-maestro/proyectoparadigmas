package com.proyectoparadigmas.graphics;

import com.proyectoparadigmas.fxutils.Criteria;

import java.util.ArrayList;

/**
 * Created by felipe on 18-12-16.
 */
public class GraphicsList<T> extends ArrayList<T> {
    private Criteria<T, T> mCriteria;

    public GraphicsList() {

    }

    public void setCriteria(Criteria<T, T> criteria) {
        mCriteria = criteria;
    }

    public T queryArea(T src) {
        if (mCriteria != null) {
            for (T dest : this) {
                if (mCriteria.evaluate(src, dest) && (src != dest)) return dest;
            }
        }

        return null;
    }
}
