package com.proyectoparadigmas.view;

import com.proyectoparadigmas.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by felipe on 22-12-16.
 */
public class RootController {

    private Stage primaryStage;

    @FXML
    private Button enter;

    @FXML
    private Button exit;

    public RootController() {}

    @FXML
    private void initialize() {

    }

    @FXML
    private void handleEnter() {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/gameCenter.fxml"));
            primaryStage.getScene().setRoot(loader.load());
            GameCenterController controller = loader.getController();
            controller.setStage(primaryStage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setStage(Stage stage) {
        primaryStage = stage;
    }

    @FXML
    private void handleExit() {
        System.exit(0);
    }
}
