package com.proyectoparadigmas.view;

import com.proyectoparadigmas.Main;
import com.proyectoparadigmas.game.ElevenMatchGame;
import com.proyectoparadigmas.game.FiveMatchGame;
import com.proyectoparadigmas.game.OneMatchGame;
import com.proyectoparadigmas.game.SevenRectGame;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by felipe on 22-12-16.
 */
public class GameCenterController {

    private Stage primaryStage;

    @FXML
    private Button elevenGameBtn;

    @FXML
    private Label elevenGameLbl;

    @FXML
    private Button sevenGameBtn;

    @FXML
    private Button oneGameBtn;

    @FXML
    private Label oneGameLbl;

    @FXML
    private Button fiveGameBtn;

    @FXML
    private Label fiveGameLbl;

    @FXML
    private Label sevenGameLbl;

    @FXML
    private Button exit;

    @FXML
    private void initialize() {
        elevenGameLbl.setText("Retire 6 Fosforos\npara formar la palabra \n\"Once\"");
        oneGameLbl.setText("Retire un solo Fosforo para\nformar 3 cuadrados iguales");
        sevenGameLbl.setText("Mueva 7 palitos para formar\n7 cuadrados\n(Rectangulo gris)");
        fiveGameLbl.setText("Arrastre los fosforos para\nformar 5 cuadrados iguales");
    }

    @FXML
    private void handleExit() {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/root.fxml"));
            primaryStage.getScene().setRoot(loader.load());
            RootController controller = loader.getController();
            controller.setStage(primaryStage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleElevenGameButton() {
        ElevenMatchGame game = new ElevenMatchGame(primaryStage.getMaxWidth(), primaryStage.getMaxHeight(), primaryStage);
        game.start();
    }

    @FXML
    private void handleSevenGameButton() {
        SevenRectGame game = new SevenRectGame(primaryStage.getMaxWidth(), primaryStage.getMaxHeight(), primaryStage);
        game.start();
    }

    @FXML
    private void handleOneGameButton() {
        OneMatchGame game = new OneMatchGame(primaryStage.getMaxWidth(), primaryStage.getMaxHeight(), primaryStage);
        game.start();
    }

    @FXML
    private void handleFiveGameButton() {
        FiveMatchGame game = new FiveMatchGame(primaryStage.getMaxWidth(), primaryStage.getMaxHeight(), primaryStage);
        game.start();
    }

    public void setStage(Stage stage) {
        primaryStage = stage;
    }
}
