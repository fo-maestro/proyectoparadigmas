package com.proyectoparadigmas.fxutils;

/**
 * Interface Callback para el despacho de cualquier evento de JavaFx.
 */
public interface EventDispatchListener<T, K> {

    /**
     * Method Callback que posee la accion a realizar al momento de disparar un evento de JavaFx.
     * @param event Evento invocado
     * @param object Objecto que invoco el evento
     */
    void onDispatch(T event, K object);
}
