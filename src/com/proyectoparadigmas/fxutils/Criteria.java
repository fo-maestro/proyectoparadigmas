package com.proyectoparadigmas.fxutils;

/**
 * Created by felipe on 18-12-16.
 */
public interface Criteria<T, K> {
    boolean evaluate(T src, K dest);
}
