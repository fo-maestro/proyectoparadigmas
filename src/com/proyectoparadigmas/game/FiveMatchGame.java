package com.proyectoparadigmas.game;

import com.proyectoparadigmas.fxutils.Criteria;
import com.proyectoparadigmas.fxutils.EventDispatchListener;
import com.proyectoparadigmas.graphics.GraphicsList;
import com.proyectoparadigmas.graphics.ItemMatch;
import com.proyectoparadigmas.graphics.ItemRectangle;
import com.proyectoparadigmas.graphics.VoidMatch;
import javafx.geometry.Bounds;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Created by felipe on 22-12-16.
 */
public class FiveMatchGame extends Game {

    private GraphicsList<ItemMatch> mMatches;
    private GraphicsList<ItemMatch> voidItems;
    private EventDispatchListener<MouseEvent, ItemMatch> mEventDispatch;
    private double prevPosX, prevPosY;

    /**
     * Crea una nueva instancia de Game.
     *
     * @param width               Ancho del Game Frame
     * @param height              Largo del Game Frame
     * @param stage               Referencia a la Primary Stage
     */
    public FiveMatchGame(double width, double height, Stage stage) {
        super(width, height,755, 570, stage);
        mMatches = new GraphicsList<>();
        voidItems = new GraphicsList<>();
        createEventDispatch();
        Criteria<ItemMatch, ItemMatch> criteria = ((src, dest) -> {
            double x = src.getX() + src.getWidth() / 2;
            double y = src.getY() + src.getHeight() / 2;
            Bounds bounds = dest.getBounds();
            return  ((x >= bounds.getMinX() && x <= bounds.getMaxX())
                    &&
                    (y >= bounds.getMinY() && y <= bounds.getMaxY()));
        });

        mMatches.setCriteria(criteria);
        voidItems.setCriteria(criteria);
    }

    private void createEventDispatch() {
        mEventDispatch = ((event, object) -> {
            object.toFront();
            object.setX(event.getX() - object.getWidth() / 2);
            object.setY(event.getY() - object.getHeight() / 2);
        });
    }

    @Override
    protected void createScene(AnchorPane gameContainer) {
        ItemMatch var;

        double x, y, matchWidth = 15, matchHeight = 150, offset = 7.5;

        x = 10; y = 20;

        voidItems.add(var = new ItemMatch(x - offset, y, new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() + offset, y,
                new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        x += 40 + var.getHeight();

        voidItems.add(var = new ItemMatch(x -offset, y, new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() + offset, y,
                new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        x += 40 + var.getHeight();

        voidItems.add(var = new ItemMatch(x -offset, y, new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() + offset, y,
                new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        x += 40 + var.getHeight();

        voidItems.add(var = new ItemMatch(x -offset, y, new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() + offset, y,
                new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        x = 10; y += 40 + var.getHeight();

        voidItems.add(var = new ItemMatch(x - offset, y, new VoidMatch(matchWidth, matchHeight)));

        mMatches.add(var = new ItemMatch(x - offset, y, matchWidth, matchHeight));

        addListeners(var);

        voidItems.add(var = new ItemMatch(x + var.getHeight() + offset, y,
                new VoidMatch(matchWidth, matchHeight)));

        mMatches.add(var = new ItemMatch(x + var.getHeight() + offset, y, matchWidth, matchHeight));

        addListeners(var);

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        mMatches.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                                            matchWidth, matchHeight, 90));

        addListeners(var);

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        mMatches.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                matchWidth, matchHeight, 90));

        addListeners(var);

        x += 40 + var.getHeight();

        voidItems.add(var = new ItemMatch(x - offset, y, new VoidMatch(matchWidth, matchHeight)));

        mMatches.add(var = new ItemMatch(x - offset, y, matchWidth, matchHeight));

        addListeners(var);

        voidItems.add(var = new ItemMatch(x + var.getHeight() + offset, y,
                new VoidMatch(matchWidth, matchHeight)));

        mMatches.add(var = new ItemMatch(x + var.getHeight() + offset, y, matchWidth, matchHeight));

        addListeners(var);

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        mMatches.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                matchWidth, matchHeight, 90));

        addListeners(var);

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        mMatches.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                matchWidth, matchHeight, 90));

        x += 40 + var.getHeight();

        voidItems.add(var = new ItemMatch(x - offset, y, new VoidMatch(matchWidth, matchHeight)));

        mMatches.add(var = new ItemMatch(x - offset, y, matchWidth, matchHeight));

        addListeners(var);

        voidItems.add(var = new ItemMatch(x + var.getHeight() + offset, y,
                new VoidMatch(matchWidth, matchHeight)));

        mMatches.add(var = new ItemMatch(x + var.getHeight() + offset, y, matchWidth, matchHeight));

        addListeners(var);

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        mMatches.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                matchWidth, matchHeight, 90));

        addListeners(var);

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        mMatches.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                matchWidth, matchHeight, 90));

        addListeners(var);

        x += 40 + var.getHeight();

        voidItems.add(var = new ItemMatch(x - offset, y, new VoidMatch(matchWidth, matchHeight)));

        mMatches.add(var = new ItemMatch(x - offset, y, matchWidth, matchHeight));

        addListeners(var);

        voidItems.add(var = new ItemMatch(x + var.getHeight() + offset, y,
                new VoidMatch(matchWidth, matchHeight)));

        mMatches.add(var = new ItemMatch(x + var.getHeight() + offset, y, matchWidth, matchHeight));

        addListeners(var);

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        mMatches.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                matchWidth, matchHeight, 90));

        addListeners(var);

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        mMatches.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                matchWidth, matchHeight, 90));

        addListeners(var);

        x = 10; y += 40 + var.getHeight();

        voidItems.add(var = new ItemMatch(x - offset, y, new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() + offset, y, new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        x += 40 + var.getHeight();

        voidItems.add(var = new ItemMatch(x -offset, y, new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() + offset, y, new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        x += 40 + var.getHeight();

        voidItems.add(var = new ItemMatch(x -offset, y, new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() + offset, y, new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        x += 40 + var.getHeight();

        voidItems.add(var = new ItemMatch(x -offset, y, new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() + offset, y, new VoidMatch(matchWidth, matchHeight)));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        voidItems.add(var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                new VoidMatch(matchWidth, matchHeight), 90));

        gameContainer.setTranslateY(-30);

        gameContainer.getChildren().addAll(voidItems);
        gameContainer.getChildren().addAll(mMatches);
    }

    private void addListeners(ItemMatch item) {
        item.setOnMousePressed((event, object) -> {
            prevPosX = object.getX();
            prevPosY = object.getY();
        });

        item.setOnMouseDragged(mEventDispatch);

        item.setOnMouseReleased((event, object) -> {
            ItemMatch val = mMatches.queryArea(object);
            if (val == null) {
                val = voidItems.queryArea(object);
                if (val != null) {
                    if ((object.getRotate() == 0 && val.getRotate() == 90) || (object.getRotate() == 90 && val.getRotate() == 0)) {
                        if (object.getRotate() == 90) {
                            object.setRotate(-90);
                        } else {
                            object.setRotate(90);
                        }
                    }
                    object.setX(val.getX());
                    object.setY(val.getY());
                } else {
                    object.setX(prevPosX);
                    object.setY(prevPosY);
                }
            } else {
                object.setX(prevPosX);
                object.setY(prevPosY);
            }
        });
    }

    @Override
    protected boolean isWinGame() {
        return false;
    }
}
