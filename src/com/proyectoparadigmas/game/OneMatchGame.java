package com.proyectoparadigmas.game;

import com.proyectoparadigmas.fxutils.EventDispatchListener;
import com.proyectoparadigmas.graphics.ItemMatch;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Created by felipe on 22-12-16.
 */
public class OneMatchGame extends Game {

    private ItemMatch[] mMatches;
    private EventDispatchListener<MouseEvent, ItemMatch> mEventDispatch;
    /**
     * Crea una nueva instancia de Game.
     *
     * @param width               Ancho del Game Frame
     * @param height              Largo del Game Frame
     * @param stage               Referencia a la Primary Stage
     */
    public OneMatchGame(double width, double height, Stage stage) {
        super(width, height, 515, 350, stage);
        mMatches = new ItemMatch[13];
        createEventDispatch();
    }

    private void createEventDispatch() {
        mEventDispatch = ((event, object) -> {
            object.setVisible(false);
            save();
            running = false;
            infoDialog.show();
        });
    }

    @Override
    protected void createScene(AnchorPane gameContainer) {
        ItemMatch var;

        double x, y, matchWidth = 15, matchHeight = 150, offset = 7.5;

        x = 10; y = 182;

        mMatches[0] = (var = new ItemMatch(x - offset, y, matchWidth, matchHeight));
        addListeners(var);

        mMatches[1] = (var = new ItemMatch(x + var.getHeight() + offset, y, matchWidth, matchHeight));
        addListeners(var);

        mMatches[2] = (var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset, matchWidth, matchHeight, 90));
        addListeners(var);

        mMatches[3] = (var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset, matchWidth, matchHeight, 90));
        addListeners(var);

        x += var.getHeight() + offset * 2;

        mMatches[4] = (var = new ItemMatch(x + var.getHeight() + offset, y , matchWidth, matchHeight));
        addListeners(var);

        mMatches[5] = (var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset, matchWidth, matchHeight, 90));
        addListeners(var);

        mMatches[6] = (var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset, matchWidth, matchHeight, 90));
        addListeners(var);

        x += var.getHeight() + offset * 2;

        mMatches[7] = (var = new ItemMatch(x + var.getHeight() + offset, y , matchWidth, matchHeight));
        addListeners(var);

        mMatches[8] = (var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset, matchWidth, matchHeight, 90));
        addListeners(var);

        mMatches[9] = (var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset, matchWidth, matchHeight, 90));
        addListeners(var);

        x -= var.getHeight() + offset * 2; y -= var.getHeight() + offset * 2;

        mMatches[10] = (var = new ItemMatch(x - offset, y , matchWidth, matchHeight));
        addListeners(var);

        mMatches[11] = (var = new ItemMatch(x + var.getHeight() + offset, y , matchWidth, matchHeight));
        addListeners(var);

        mMatches[12] = (var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset, matchWidth, matchHeight, 90));
        addListeners(var);

        gameContainer.getChildren().addAll(mMatches);
    }

    private void addListeners(ItemMatch match) {
        match.setOnMouseClicked(mEventDispatch);
    }

    @Override
    protected boolean isWinGame() {
        return !mMatches[6].isVisible();
    }
}
