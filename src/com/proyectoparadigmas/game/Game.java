package com.proyectoparadigmas.game;

import com.proyectoparadigmas.Main;
import com.proyectoparadigmas.view.GameCenterController;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * Clase base para la creacion de videojuegos en JavaFx.
 */
public abstract class Game {

    /**
     * Ancho del Game Frame.
     */
    protected double width;

    /**
     * Largo del Game Frame.
     */
    protected double height;

    /**
     * Referencia a la Primary Stage para la creacion del Game Frame.
     */
    protected Stage mStage;

    /**
     * Variable que indica si el juego se esta ejecutando.
     */
    protected boolean running;

    protected int tries;
    protected int count;
    protected boolean restart;
    protected Service<Void> gameService;
    protected Alert infoDialog;
    protected int minutesValue, secondsValue;
    protected int acumMinutes, acumSeconds;
    protected GameLayout layout;
    private double gameContainerWidth;
    private double gameContainerHeight;
    private BorderPane root;
    private AnchorPane gameContainer;
    /**
     * Crea una nueva instancia de Game.
     * @param width Ancho del Game Frame
     * @param height Largo del Game Frame
     * @param stage Referencia a la Primary Stage
     */
    protected Game(double width, double height, double gameContainerWidth, double gameContainerHeight, Stage stage) {
        this.width = width;
        this.height = height;
        this.gameContainerWidth = gameContainerWidth;
        this.gameContainerHeight = gameContainerHeight;
        mStage = stage;
        init();
        createInfoDialog();
        createService();
    }

    private void init() {
        tries = 0;
        count = 0;
        secondsValue = 0;
        minutesValue = 0;
        acumMinutes = 0;
        acumSeconds = 0;
        root = new BorderPane();
    }

    private void createInfoDialog() {
        infoDialog = new Alert(Alert.AlertType.NONE);
        infoDialog.initStyle(StageStyle.UNDECORATED);
        infoDialog.getDialogPane().setContentText("Please Wait...");
        infoDialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        Node close = infoDialog.getDialogPane().lookupButton(ButtonType.CLOSE);
        close.managedProperty().bind(close.visibleProperty());
        close.setVisible(false);
    }

    private void createService() {
        restart = false;
        gameService = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        while (running) {
                            if (secondsValue == 60) {
                                minutesValue++;
                                secondsValue = 0;
                            }
                            Platform.runLater(() -> layout.setTime(minutesValue, secondsValue++));
                            Thread.sleep(1000);
                        }
                        return null;
                    }
                };
            }
        };

        gameService.setOnSucceeded(event -> {
            infoDialog.close();

            if (restart) {
                restart = false;
                Platform.runLater(() -> layout.setTries(++tries));
                restart();

                return;
            }

            if (isWinGame()) {
                Alert win = new Alert(Alert.AlertType.NONE);
                win.initStyle(StageStyle.UNDECORATED);
                fixTime();
                String message = "Has ganado!!\n\n" +
                        "Resultado final:\n" +
                        "Tiempo Total: " + ((acumMinutes >= 10) ? acumMinutes : "0" + acumMinutes) +
                        ":" + ((acumSeconds >= 10) ? acumSeconds : "0" + acumSeconds) + "\n" +
                        "Numero de Intentos: " + tries;
                win.getDialogPane().setContentText(message);
                ButtonType exit = new ButtonType("Salir");
                win.getButtonTypes().add(exit);

                Optional<ButtonType> result = win.showAndWait();

                if (result.isPresent()) {
                    win.close();
                    stop();
                }
            } else {
                Alert lose = new Alert(Alert.AlertType.NONE);
                lose.initStyle(StageStyle.UNDECORATED);
                lose.getDialogPane().setContentText("Has Perdido. ¿Deseas Reintentarlo otra vez?");
                ButtonType restart = new ButtonType("Reiniciar");
                ButtonType exit = new ButtonType("Salir");
                lose.getButtonTypes().addAll(restart, exit);
                Optional<ButtonType> result = lose.showAndWait();

                if (result.get() == restart) {
                    Platform.runLater(() -> layout.setTries(++tries));
                    restart();
                } else {
                    lose.close();
                    stop();
                }
            }
        });
    }

    private void fixTime() {
        acumSeconds -= (tries + 1);
        if (acumSeconds >= 60) {
            acumMinutes += acumSeconds / 60;
            acumSeconds = acumSeconds % 60;
        }
    }

    /**
     * Obtiene el Ancho del Game Frame.
     * @return Ancho del Game Frame
     */
    public double getWidth() { return width; }

    /**
     * Obtiene el Largo del Game Frame.
     * @return Largo del Game Frame
     */
    public double getHeight() { return height; }

    /**
     * Redimensiona el Ancho del Game Frame.
     * @param width Ancho del Game Frame
     */
    public void setWidth(double width) { this.width = width; }

    /**
     * Redimensiona el Largo del Game Frame.
     * @param height Largo del Game Frame
     */
    public void setHeight(double height) { this.height = height; }

    /**
     * Obtiene la referencia a la Primary Stage.
     * @return Primary Stage
     */
    public Stage getStage() { return mStage; }

    /**
     * Comienza la ejecucion del juego. Crea la Escena principal llamando al metodo createScene.
     */
    public void start() {
        root.getStylesheets().add(Main.class.getResource("view/root.css").toExternalForm());
        root.getStyleClass().add("game-background");
        createGameLayout();
        mStage.getScene().setRoot(createScene());
        running = true;
        gameService.start();
    }

    /**
     * Detiene la ejecucion del juego. Crea una Escena en Blanco.
     */
    public void stop() {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/gameCenter.fxml"));

        try {
            mStage.getScene().setRoot(loader.load());
            GameCenterController controller = loader.getController();
            controller.setStage(mStage);
        } catch (IOException e) {
            e.printStackTrace();
        }
        running = false;
    }

    /**
     * Resetea todas las variables de estado del juego.
     */
    public void reset() {
        gameService.reset();
        tries = 0;
        count = 0;
        secondsValue = 0;
        minutesValue = 0;
        acumMinutes = 0;
        acumSeconds = 0;
    }

    /**
     * Resetea las variables de estado menos los acumuladores de tiempo e tries, y comienza la ejecucion del juego.
     */
    public void restart() {
        gameService.reset();
        count = 0;
        secondsValue = 0;
        minutesValue = 0;
        createScene(createGameContainer());
        running = true;
        gameService.start();
    }

    private void createGameLayout() {
        layout = new GameLayout(root, width);
        layout.setExitButtonHandler(event -> {
            gameService.cancel();
            stop();
        });

        layout.setRestartButtonHandler(event -> {
            save();
            restart = true;
            running = false;
            infoDialog.show();
        });
    }

    /**
     * Guarda el tiempo Actual acumulado.
     */
    public void save() {
        acumMinutes += minutesValue;
        acumSeconds += secondsValue;
    }

    /**
     * Obtiene el estado actual del juego.
     * @return Variable de estado de ejecucion
     */
    public boolean isRunning() {
        return running;
    }

    private BorderPane createScene() {
        createScene(createGameContainer());
        return root;
    }

    private AnchorPane createGameContainer() {
        gameContainer = new AnchorPane();
        root.setCenter(gameContainer);
        gameContainer.setMinSize(gameContainerWidth, gameContainerHeight);
        gameContainer.setPrefSize(gameContainerWidth, gameContainerHeight);
        gameContainer.setMaxSize(gameContainerWidth, gameContainerHeight);

        return gameContainer;
    }

    /**
     * Crea la Escena principal del Game Frame
     * @return Escena principal
     */
    protected abstract void createScene(AnchorPane gameContainer);

    /**
     * Metodo que determina si las jugadas realizadas corresponden a un juego ganado.
     * @return Resultado final del Juego (Gana o Pierde)
     */
    protected abstract boolean isWinGame();

}
