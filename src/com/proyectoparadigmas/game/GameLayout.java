package com.proyectoparadigmas.game;


import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Created by felipe on 17-12-16.
 */
public class GameLayout {

    private Label timeLabel;
    private Label triesLabel;
    private Button exit;
    private Button restart;


    public GameLayout(BorderPane root, double width) {
        root.setBackground(Background.EMPTY);
        VBox stats = new VBox();
        timeLabel = new Label("Tiempo: 00:00");
        timeLabel.setFont(new Font(timeLabel.getFont().getName(), 20));
        timeLabel.setTextFill(Color.WHITE);
        triesLabel = new Label("Intentos: 0");
        triesLabel.setFont(new Font(triesLabel.getFont().getName(), 20));
        triesLabel.setTextFill(Color.WHITE);
        stats.setPadding(new Insets(10, 10, 10, 10));
        stats.getChildren().addAll(timeLabel, triesLabel);

        root.setTop(stats);
        BorderPane pan = new BorderPane();
        pan.setPrefWidth(width);
        pan.setPrefHeight(75);
        exit = new Button("Salir");
        restart = new Button("Reiniciar");

        HBox buttonBox = new HBox();
        buttonBox.setSpacing(5);
        buttonBox.getChildren().addAll(exit, restart);
        buttonBox.setAlignment(Pos.BASELINE_CENTER);
        pan.setCenter(buttonBox);

        root.setBottom(pan);
    }

    public void setTime(int minutes, int seconds) {
        timeLabel.setText("Tiempo: " + ((minutes >= 10) ? minutes : "0" + minutes) +
                            ":" +
                            ((seconds >= 10) ?  seconds : "0" + seconds));
    }

    public void setTries(int tries) {
        triesLabel.setText("Intentos: " + tries);
    }

    public void setExitButtonHandler(EventHandler<? super MouseEvent> listener) {
        exit.setOnMouseClicked(listener);
    }

    public void setRestartButtonHandler(EventHandler<? super MouseEvent> listener) {
        restart.setOnMouseClicked(listener);
    }
}
