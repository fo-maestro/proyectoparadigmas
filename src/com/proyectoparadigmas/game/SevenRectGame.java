package com.proyectoparadigmas.game;

import com.proyectoparadigmas.fxutils.Criteria;
import com.proyectoparadigmas.fxutils.EventDispatchListener;
import com.proyectoparadigmas.graphics.GraphicsList;
import com.proyectoparadigmas.graphics.ItemRectangle;
import javafx.geometry.Bounds;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Created by felipe on 17-12-16.
 */
public class SevenRectGame extends Game {

    private GraphicsList<ItemRectangle> mRectangles;
    private GraphicsList<ItemRectangle> voidItems;
    private EventDispatchListener<MouseEvent, ItemRectangle> mEventDispatch;
    private double prevPosX, prevPosY;

    /**
     * Crea una nueva instancia de Game.
     *
     * @param width  Ancho del Game Frame
     * @param height Largo del Game Frame
     * @param stage  Referencia a la Primary Stage
     */
    public SevenRectGame(double width, double height, Stage stage) {
        super(width, height, 515, 350, stage);
        mRectangles = new GraphicsList<>();
        voidItems = new GraphicsList<>();
        createEventDispath();
        Criteria<ItemRectangle, ItemRectangle> criteria = ((src, dest) -> {
            double x = src.getX() + src.getWidth() / 2;
            double y = src.getY() + src.getHeight() / 2;
            Bounds bounds = dest.getBounds();
            return  ((x >= bounds.getMinX() && x <= bounds.getMaxX())
                    &&
                    (y >= bounds.getMinY() && y <= bounds.getMaxY()));
        });

        mRectangles.setCriteria(criteria);
        voidItems.setCriteria(criteria);
    }

    private void createEventDispath() {
        mEventDispatch = ((event, object) -> {
            object.toFront();
            object.setX(event.getX() - object.getWidth() / 2);
            object.setY(event.getY() - object.getHeight() / 2);
        });
    }

    @Override
    protected void createScene(AnchorPane gameContainer) {
        ItemRectangle var;
        double x, y, rectangleWidth = 15, rectangleHeight = 150, offset = 7.5;

        x = 10; y = 182;

        //0 left
        voidItems.add(var = new ItemRectangle(x - offset, y, rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);

        mRectangles.add(var = new ItemRectangle(x - offset, y, rectangleWidth, rectangleHeight));
        var.setFill(Color.RED);
        addListeners(var);

        //1 right
        voidItems.add(var = new ItemRectangle(x + var.getHeight() + offset, y, rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);

        mRectangles.add(var = new ItemRectangle(x + var.getHeight() + offset, y, rectangleWidth, rectangleHeight));
        var.setFill(Color.BLUE);
        addListeners(var);

        //2 up
        voidItems.add(var = new ItemRectangle(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);
        var.rotate();

        mRectangles.add(var = new ItemRectangle(x + var.getWidth() / 2, y - var.getWidth() / 2 - offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.SKYBLUE);
        var.rotate();
        addListeners(var);

        //3 down
        voidItems.add(var = new ItemRectangle(x + var.getWidth() / 2, y + var.getWidth() / 2 + offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);
        var.rotate();

        mRectangles.add(var = new ItemRectangle(x + var.getWidth() / 2, y + var.getWidth() / 2 + offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.DARKGREEN);
        var.rotate();
        addListeners(var);

        x += var.getWidth() + offset * 2;

        voidItems.add(var = new ItemRectangle(x + var.getWidth() + offset, y , rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);

        mRectangles.add(var = new ItemRectangle(x + var.getHeight() + offset, y , rectangleWidth, rectangleHeight));
        var.setFill(Color.ORANGE);
        addListeners(var);

        voidItems.add(var = new ItemRectangle(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);
        var.rotate();

        mRectangles.add(var = new ItemRectangle(x + var.getWidth() / 2, y - var.getWidth() / 2 - offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.GREEN);
        var.rotate();
        addListeners(var);

        voidItems.add(var = new ItemRectangle(x + var.getWidth() / 2, y + var.getWidth() / 2 + offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);
        var.rotate();

        //mRectangles.add(new ItemRectangle(0,0, 0, 0));

        x += var.getWidth() + offset * 2;

        voidItems.add(var = new ItemRectangle(x + var.getWidth() + offset, y , rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);

        mRectangles.add(var = new ItemRectangle(x + var.getHeight() + offset, y , rectangleWidth, rectangleHeight));
        var.setFill(Color.PINK);
        addListeners(var);

        voidItems.add(var = new ItemRectangle(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);
        var.rotate();

        mRectangles.add(var = new ItemRectangle(x + var.getWidth() / 2, y - var.getWidth() / 2 - offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.YELLOW);
        var.rotate();
        addListeners(var);

        voidItems.add(var = new ItemRectangle(x + var.getWidth() / 2, y + var.getWidth() / 2 + offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);
        var.rotate();

        mRectangles.add(var = new ItemRectangle(x + var.getWidth() / 2, y + var.getWidth() / 2 + offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.DARKORANGE);
        var.rotate();
        addListeners(var);

        x -= var.getWidth() + offset * 2; y -= var.getWidth() + offset * 2;

        voidItems.add(var = new ItemRectangle(x - offset, y , rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);

        mRectangles.add(var = new ItemRectangle(x - offset, y , rectangleWidth, rectangleHeight));
        var.setFill(Color.BROWN);
        addListeners(var);

        voidItems.add(var = new ItemRectangle(x + var.getHeight() + offset, y , rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);

        mRectangles.add(var = new ItemRectangle(x + var.getHeight() + offset, y , rectangleWidth, rectangleHeight));
        var.setFill(Color.BLUE);
        addListeners(var);

        voidItems.add(var = new ItemRectangle(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.LIGHTGRAY);
        var.rotate();

        mRectangles.add(var = new ItemRectangle(x + var.getWidth() / 2, y - var.getWidth() / 2 - offset, rectangleWidth, rectangleHeight));
        var.setFill(Color.PINK);
        var.rotate();
        addListeners(var);

        gameContainer.getChildren().addAll(voidItems);
        gameContainer.getChildren().addAll(mRectangles);
    }

    private void addListeners(ItemRectangle item) {
        item.setOnMousePressed((event, object) ->{
            prevPosX = object.getX();
            prevPosY = object.getY();
        });
        item.setOnMouseDragged(mEventDispatch);
        item.setOnMouseReleased((event, object) -> {
            ItemRectangle val = mRectangles.queryArea(object);
            if (val == null) {
                val = voidItems.queryArea(object);
                if (val != null) {
                    if ((object.getRotation() == 0 && val.getRotation() == 90) || (object.getRotation() == 90 && val.getRotation() == 0)) {
                        object.rotate();
                    }
                    object.setX(val.getX());
                    object.setY(val.getY());
                } else {
                    object.setX(prevPosX);
                    object.setY(prevPosY);
                }
            } else {
                object.setX(prevPosX);
                object.setY(prevPosY);
            }
        });
    }

    @Override
    public void reset() {
        super.reset();
        mRectangles = new GraphicsList<>();
    }

    @Override
    public void restart() {
        mRectangles = new GraphicsList<>();
        super.restart();
    }

    @Override
    protected boolean isWinGame() {
        return false;
    }
}
