package com.proyectoparadigmas.game;

import com.proyectoparadigmas.fxutils.EventDispatchListener;
import com.proyectoparadigmas.graphics.ItemMatch;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * Clase para la creacion de un Juego matematico(Fosforos)
 */
public class ElevenMatchGame extends Game{

    private ItemMatch[] mMatches;
    private boolean[] win;
    private EventDispatchListener<MouseEvent, ItemMatch> mEventDispatch;

    /**
     * Crea una nueva instancia de ElevenMatchGame.
     * @param width Ancho del Juego
     * @param height Alto del Juego
     * @param stage Primary Stage
     */
    public ElevenMatchGame(double width, double height, Stage stage) {
        super(width, height, 875, 190, stage);
        createEventDispatch();
        mMatches = new ItemMatch[20];
        initWin();
    }

    private void createEventDispatch() {
        mEventDispatch = ((event, object) -> {
            object.setVisible(false);
            count++;

            if (count == 6) {
                save();
                running = false;
                infoDialog.show();
            }
        });
    }

    private void initWin() {
        win = new boolean[20];

        for (int i = 0; i < 20; i++) win[i] = true;

        win[4] = false;
        win[7] = false;
        win[8] = false;
        win[11] = false;
        win[14] = false;
        win[16] = false;
    }

    /**
     * Crea la escena principal del Juego.
     * @return Escena del Juego
     */
    @Override
    protected void createScene(AnchorPane gameContainer) {
        ItemMatch var;
        double x, y, matchWidth = 15, matchHeight = 150, offset = 7.5;

        x = 10; y = 20;

        //0 left
        mMatches[0] = (var = new ItemMatch(x - offset, y, matchWidth, matchHeight));
        addListeners(var);

        //1 right
        mMatches[1] = (var = new ItemMatch(x + var.getHeight() + offset, y, matchWidth, matchHeight));
        addListeners(var);

        //2 up
        mMatches[2] = (var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                                        matchWidth, matchHeight, -90));
        addListeners(var);

        //3 down
        mMatches[3] = (var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                                        matchWidth, matchHeight, -90));
        addListeners(var);

        //4 center
        mMatches[4] = (var = new ItemMatch(x + var.getHeight() / 2, y, matchWidth, matchHeight, 180));
        addListeners(var);

        x += 80 + var.getHeight();

        //5 left
        mMatches[5] = (var = new ItemMatch(x - offset, y, matchWidth, matchHeight));
        addListeners(var);

        //6 right
        mMatches[6] = (var = new ItemMatch(x + var.getHeight() + offset, y, matchWidth, matchHeight));
        addListeners(var);

        //7 up
        mMatches[7] = (var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                matchWidth, matchHeight, -90));
        addListeners(var);

        //8 down
        mMatches[8] = (var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                matchWidth, matchHeight, -90));
        addListeners(var);

        //9 center
        mMatches[9] = (var = new ItemMatch(x + var.getHeight() / 2, y, matchWidth, matchHeight, 135));
        addListeners(var);

        x += 80 + var.getHeight();

        //10 left
        mMatches[10] = (var = new ItemMatch(x - offset, y, matchWidth, matchHeight));
        addListeners(var);

        //11 right
        mMatches[11] = (var = new ItemMatch(x + var.getHeight() + offset, y, matchWidth, matchHeight));
        addListeners(var);

        //12 up
        mMatches[12] = (var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                matchWidth, matchHeight, -90));
        addListeners(var);

        //13 down
        mMatches[13] = (var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                matchWidth, matchHeight, -90));
        addListeners(var);

        //14 center
        mMatches[14] = (var = new ItemMatch(x + var.getHeight() / 2, y, matchWidth, matchHeight, 225));
        addListeners(var);

        x += 80 + var.getHeight();

        //15 left
        mMatches[15] = (var = new ItemMatch(x - offset, y, matchWidth, matchHeight));
        addListeners(var);

        //16 right
        mMatches[16] = (var = new ItemMatch(x + var.getHeight() + offset, y, matchWidth, matchHeight));
        addListeners(var);

        //17 up
        mMatches[17] = (var = new ItemMatch(x + var.getHeight() / 2, y - var.getHeight() / 2 - offset,
                matchWidth, matchHeight, -90));
        addListeners(var);

        //18 down
        mMatches[18] = (var = new ItemMatch(x + var.getHeight() / 2, y + var.getHeight() / 2 + offset,
                matchWidth, matchHeight, -90));
        addListeners(var);

        //19 center
        mMatches[19] = (var = new ItemMatch(x + var.getHeight() / 2, y, matchWidth, matchHeight, 90));
        addListeners(var);
        gameContainer.getChildren().addAll(mMatches);
    }

    /**
     * Resetea todas las variables de estado del juego.
     */
    public void reset() {
        super.reset();
        mMatches = new ItemMatch[20];
    }

    /**
     * Resetea las variables de estado menos los acumuladores de tiempo e tries, y comienza la ejecucion del juego.
     */
    public void restart() {
        mMatches = new ItemMatch[20];
        super.restart();
    }

    /**
     * Metodo que determina si las jugadas realizadas corresponden a un juego ganado.
     * @return Resultado final del Juego (Gana o Pierde)
     */
    @Override
    protected boolean isWinGame() {
        for (int i = 0; i < mMatches.length; i++) {
            if (mMatches[i].isVisible() != win[i]) return false;
        }

        return true;
    }

    private void addListeners(ItemMatch var) {
        var.setOnMouseClicked(mEventDispatch);
    }
}
