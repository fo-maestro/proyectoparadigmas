package com.proyectoparadigmas;

import com.proyectoparadigmas.game.ElevenMatchGame;
import com.proyectoparadigmas.game.FiveMatchGame;
import com.proyectoparadigmas.game.OneMatchGame;
import com.proyectoparadigmas.view.RootController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Created by felipe on 22-10-16.
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Game");
        primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        primaryStage.setFullScreen(true);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/root.fxml"));
        BorderPane root = loader.load();
        RootController rootController = loader.getController();
        rootController.setStage(primaryStage);
        primaryStage.setScene(new Scene(root));
        primaryStage.setOnCloseRequest(we -> System.exit(0));
        primaryStage.show();

    }

    public static void main(String[] args) { launch(args); }
}
